# AWS Lambda CI/CD 
## _using Terraform and CodePipeline_

This example will help you to setup fully functioning AWS Lambda sample service through  continous integration and deployment using CodePipeline and Terraform.

## Prerequiste 

- [AWS Cli][awscli] installed
- [Terraform Cli][tf] installed 
- ✨Magic ✨

[awscli]: https://aws.amazon.com/cli/
[tf]: https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli

## Features

- AWS Lambda Function - Python will print an welcome message and use bs4 to get Google.com page title.
- CI/CD - Complete CodePipeline: [CodeCommit][cc], Source, Build, Deploy
- Private [Elastic Container Registry][ecr] for your AWS Lambda Code

[cc]: https://aws.amazon.com/codecommit/
[ecr]: https://aws.amazon.com/ecr/

## Instruction
Clone this repository to wherever you want
```sh
git clone https://gitlab.com/shinphil/aws-lambda-cicd-terraform.git
cd aws-lambda-cicd-terraform/terraform
```
Modify project name on `terraform.tfvars` file as you wish.

```sh
terraform init
terraform apply
# when asked, type yes if you want to apply to your AWS Account
```

When it's done, this will create
- iam, role, policy, CodeCommit, Lambda function, CodePipeline and so on

## Note
_If ECR access error found on lambda module_

Change `image_uri` to any image uri that you can access
```
resource "aws_lambda_function" "this" {
    ...
    # Initial image has to be set, without setting it, it will fail when apply.
    # this will be replaced when build an image from CodeBuild
    # image_uri       = "${var.ecr_repo_url}:latest"
    # Use any image you have an access
    image_uri       = "public.ecr.aws/lambda/python:3.8"
    ...
}
```

## Not yet! Push code to new CodeCommit Repository

You will see newly create repository, you need to push code to it.

<img src="assets/codecommit-repo.png" width="600">

```sh
# move to the directory you want to make lambda code project director
git clone https://git-codecommit.ap-northeast-2.amazonaws.com/v1/repos/phil-batch-hello_test_code_repo
# copy sample code to repo directory
cp ./aws-lambda-cicd-terraform/lambdacode/* ./phil-batch-hello_test_code_repo/
# move to cloned repo directory
cd phil-batch-hello_test_code_repo
# push sample code to repo
git add .
git commit -m "Initial commit"
git push
```

## CICD - Now ready to Build and Deploy

- On AWS Web Console, go to `CodeCommit`
- Go to `CodePipeline`

<img src="assets/codepipeline.png" width="200">

- Click on pipeline we created, hit `Release`

<img src="assets/codepipeline-detail.png" width="600">

## Checkout updated Lambda

- CodeDeploy will replace using AWS Cli with Docker image built during CodeBuild phase with source code with pushed to CodeCommit

<img src="assets/lambda.png" width="300">

- Click on lambda we created

<img src="assets/lambda-detail.png" width="650">

# Things to think about how to deploy Lambda Code

- If lambda is deployed with Image, you can't see the code directly from AWS Web Console
- There are other ways to deploy lambda code to AWS, you can zip source codes and replace it to update lambda code

## Reference
- https://github.com/aws-samples/codepipeline-for-lambda-using-terraform
- https://www.maxivanov.io/deploy-aws-lambda-to-vpc-with-terraform/

## License
MIT